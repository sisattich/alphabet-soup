package src.main.java.com.main;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class App {
    private static final String PATH_TO_FILE = "input.txt";

    public static void main( String[] args ) {
        List<String> lines;

        try {
            lines = getLinesFromFile(PATH_TO_FILE);
        } catch (FileNotFoundException e) {
            System.out.println("Could not find file at " + PATH_TO_FILE + ". Either add a file there or modify PATH_TO_FILE before running again.");
            return;
        }

        int gridHeight = Integer.valueOf(lines.get(0).substring(0, 1));
        int gridWidth = Integer.valueOf(lines.get(0).substring(2));

        char[][] grid = extractGridFromLines(lines, gridHeight, gridWidth);
        String[] words = extractWordsFromLines(lines, gridHeight);

        for (String word : words) {
            String coordinates = getWordCoordinates(word, grid);
            System.out.println(word + " " + coordinates);
        }
    }

    protected static char[][] extractGridFromLines(List<String> lines, int gridHeight, int gridWidth) {
        char[][] grid = new char[gridHeight][gridWidth];

        for (int i = 0; i < gridHeight; i ++) {
            int lineIndex = i + 1;
            for (int j = 0; j < gridWidth; j ++) {
                grid[i][j] = lines.get(lineIndex).charAt(j*2);
            }
        }

        return grid;
    }

    protected static String[] extractWordsFromLines(List<String> lines, int gridHeight) {
        int wordsStartIndex = gridHeight + 1;
        String[] words = new String[lines.size() - wordsStartIndex];
        for (int i = 0; i < words.length; i ++) {
            words[i] =  lines.get(wordsStartIndex + i);
        }
        return words;
    }

    protected static String getWordCoordinates(String word, char[][] grid) {
        String[] words = word.split(" ", 0);
        if (words.length > 1)
            return getWordCoordinates(String.join("", words), grid);

        char firstLetter = word.charAt(0);
        for (int row = 0; row < grid.length; row ++) {
            for (int col = 0; col < grid[row].length; col ++) {
                if (grid[row][col] != firstLetter)
                    continue;

                int[] startCoords = new int[]{row, col};
                for (Direction dir : Direction.values()) {
                    int[] endCoords = getWordEndCoordinates(word, startCoords, dir);
                    if (endCoords[0] >= grid.length 
                        || endCoords[1] >= grid[row].length
                        || endCoords[0] < 0
                        || endCoords[1] < 0)
                        continue;

                    String foundWord = getWord(grid, startCoords, dir, word.length());
                    if (word.toUpperCase().equals(foundWord.toUpperCase()))
                        return stringifyCoordinates(startCoords) + " " + stringifyCoordinates(endCoords);
                }
            }
        }
        return "was not found in the word search.";
    }

    private static ArrayList<String> getLinesFromFile(String pathToFile) throws FileNotFoundException {
        ArrayList<String> lines = new ArrayList<>();
        File file = new File(pathToFile);
        Scanner reader = new Scanner(file);
        while (reader.hasNextLine()) {
            String line = reader.nextLine();
            lines.add(line);
        }
        reader.close();
        return lines;
    }

    private static int[] getWordEndCoordinates(String word, int[] startCoords, Direction dir) {
        return getNeighborCoords(startCoords, dir, word.length() - 1);
    }

    private static int[] getNeighborCoords(int[] startCoords, Direction dir, int distance) {
        if (distance == 0)
            return startCoords;

        switch(dir) {
            case NORTH: return getNeighborCoords(new int[]{startCoords[0] - 1, startCoords[1]}, dir, distance - 1);
            case SOUTH: return getNeighborCoords(new int[]{startCoords[0] + 1, startCoords[1]}, dir, distance - 1);
            case EAST: return getNeighborCoords(new int[]{startCoords[0], startCoords[1] + 1}, dir, distance - 1);
            case WEST: return getNeighborCoords(new int[]{startCoords[0], startCoords[1] - 1}, dir, distance - 1);
            case NORTHEAST: return getNeighborCoords(new int[]{startCoords[0] - 1, startCoords[1] + 1}, dir, distance - 1);
            case NORTHWEST: return getNeighborCoords(new int[]{startCoords[0] - 1, startCoords[1] - 1}, dir, distance - 1);
            case SOUTHWEST: return getNeighborCoords(new int[]{startCoords[0] + 1, startCoords[1] - 1}, dir, distance - 1);
            default: return getNeighborCoords(new int[]{startCoords[0] + 1, startCoords[1] + 1}, dir, distance - 1);
        }
    }

    private static String getWord(char[][] grid, int[] startCoords, Direction dir, int wordLength) {
        return getWord(grid, startCoords, dir, wordLength, "");
    }

    private static String getWord(char[][] grid, int[] startCoords, Direction dir, int wordLength, String wordSoFar) {
        if (wordSoFar.length() == wordLength)
            return wordSoFar;
        
        String word = wordSoFar + grid[startCoords[0]][startCoords[1]];
        int[] nextCoords = getNeighborCoords(startCoords, dir, 1);

        return getWord(grid, nextCoords, dir, wordLength, word);
    }

    private static String stringifyCoordinates(int[] coords) {
        return coords[0] + ":" + coords[1];
    }

    private enum Direction {
        NORTH,
        SOUTH,
        EAST,
        WEST,
        NORTHEAST,
        SOUTHEAST,
        NORTHWEST,
        SOUTHWEST
    }
}
