package src.test.java.com.main;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import src.main.java.com.main.App;

public class AppTest extends App {
    @Test
    public void extractGridFromLinesShouldExtractSquareGrid() {
        List<String> lines = 
            List.of(
                "4x4",
                "W O R D",
                "X X X X",
                "X X X X",
                "X X X X",
                "WORD"
            );
        char[][] expectedGrid = 
            new char[][] {
                new char[] {'W', 'O', 'R', 'D'},
                new char[] {'X', 'X', 'X', 'X'},
                new char[] {'X', 'X', 'X', 'X'},
                new char[] {'X', 'X', 'X', 'X'}
            };
        assertEquals(expectedGrid, extractGridFromLines(lines, 4, 4));
    }

    @Test
    public void extractGridFromLinesShouldExtractLongRectangleGrid() {
        List<String> lines = 
            List.of(
                "4x5",
                "W O R D X",
                "X X X X X",
                "X X X X X",
                "X X X X X",
                "WORD"
            );
        char[][] expectedGrid = 
            new char[][] {
                new char[] {'W', 'O', 'R', 'D', 'X'},
                new char[] {'X', 'X', 'X', 'X', 'X'},
                new char[] {'X', 'X', 'X', 'X', 'X'},
                new char[] {'X', 'X', 'X', 'X', 'X'}
            };
        assertEquals(expectedGrid, extractGridFromLines(lines, 4, 5));
    }

    @Test
    public void extractGridFromLinesShouldExtractTallRectangleGrid() {
        List<String> lines = 
            List.of(
                "4x3",
                "W X X",
                "O X X",
                "R X X",
                "D X X",
                "WORD"
            );
        char[][] expectedGrid = 
            new char[][] {
                new char[] {'W', 'X', 'X'},
                new char[] {'O', 'X', 'X'},
                new char[] {'R', 'X', 'X'},
                new char[] {'D', 'X', 'X'}
            };
        assertEquals(expectedGrid, extractGridFromLines(lines, 4, 3));
    }

    @Test
    public void extractWordsFromLinesShouldExtractWords() {
        List<String> lines = 
            List.of(
                "4x4",
                "W O R D",
                "X X X X",
                "X X X X",
                "X X X X",
                "WORD",
                "DROW"
            );
        String[] expectedWords = new String[] {"WORD", "DROW"};
        assertEquals(expectedWords, extractWordsFromLines(lines, 4));
    }

    @Test
    public void getWordCoordinatesShouldFindWordReadingNorth() {
        char[][] grid = 
            new char[][] {
                new char[] {'X', 'X', 'X'},
                new char[] {'X', 'D', 'X'},
                new char[] {'X', 'R', 'X'},
                new char[] {'X', 'O', 'X'},
                new char[] {'X', 'W', 'X'},
                new char[] {'X', 'X', 'X'}
            };
        String expectedCoords = "4:1 1:1";
        assertEquals(expectedCoords, getWordCoordinates("WORD", grid));
    }

    @Test
    public void getWordCoordinatesShouldFindWordReadingSouth() {
        char[][] grid = 
            new char[][] {
                new char[] {'X', 'X', 'X'},
                new char[] {'X', 'W', 'X'},
                new char[] {'X', 'O', 'X'},
                new char[] {'X', 'R', 'X'},
                new char[] {'X', 'D', 'X'},
                new char[] {'X', 'X', 'X'}
            };
        String expectedCoords = "1:1 4:1";
        assertEquals(expectedCoords, getWordCoordinates("WORD", grid));
    }

    @Test
    public void getWordCoordinatesShouldFindWordReadingEast() {
        char[][] grid = 
            new char[][] {
                new char[] {'X', 'X', 'X', 'X', 'X', 'X'},
                new char[] {'X', 'W', 'O', 'R', 'D', 'X'},
                new char[] {'X', 'X', 'X', 'X', 'X', 'X'}
            };
        String expectedCoords = "1:1 1:4";
        assertEquals(expectedCoords, getWordCoordinates("WORD", grid));
    }

    @Test
    public void getWordCoordinatesShouldFindWordReadingWest() {
        char[][] grid = 
            new char[][] {
                new char[] {'X', 'X', 'X', 'X', 'X', 'X'},
                new char[] {'X', 'D', 'R', 'O', 'W', 'X'},
                new char[] {'X', 'X', 'X', 'X', 'X', 'X'}
            };
        String expectedCoords = "1:4 1:1";
        assertEquals(expectedCoords, getWordCoordinates("WORD", grid));
    }

    @Test
    public void getWordCoordinatesShouldFindWordReadingNorthEast() {
        char[][] grid = 
            new char[][] {
                new char[] {'X', 'X', 'X', 'D'},
                new char[] {'X', 'X', 'R', 'X'},
                new char[] {'X', 'O', 'X', 'X'},
                new char[] {'W', 'X', 'X', 'X'}
            };
        String expectedCoords = "3:0 0:3";
        assertEquals(expectedCoords, getWordCoordinates("WORD", grid));
    }

    @Test
    public void getWordCoordinatesShouldFindWordReadingSouthWest() {
        char[][] grid = 
            new char[][] {
                new char[] {'X', 'X', 'X', 'W'},
                new char[] {'X', 'X', 'O', 'X'},
                new char[] {'X', 'R', 'X', 'X'},
                new char[] {'D', 'X', 'X', 'X'}
            };
        String expectedCoords = "0:3 3:0";
        assertEquals(expectedCoords, getWordCoordinates("WORD", grid));
    }

    @Test
    public void getWordCoordinatesShouldFindWordReadingNorthWest() {
        char[][] grid = 
            new char[][] {
                new char[] {'D', 'X', 'X', 'X'},
                new char[] {'X', 'R', 'X', 'X'},
                new char[] {'X', 'X', 'O', 'X'},
                new char[] {'X', 'X', 'X', 'W'}
            };
        String expectedCoords = "3:3 0:0";
        assertEquals(expectedCoords, getWordCoordinates("WORD", grid));
    }

    @Test
    public void getWordCoordinatesShouldFindWordReadingSouthEast() {
        char[][] grid = 
            new char[][] {
                new char[] {'W', 'X', 'X', 'X'},
                new char[] {'X', 'O', 'X', 'X'},
                new char[] {'X', 'X', 'R', 'X'},
                new char[] {'X', 'X', 'X', 'D'}
            };
        String expectedCoords = "0:0 3:3";
        assertEquals(expectedCoords, getWordCoordinates("WORD", grid));
    }

    @Test
    public void getWordCoordinatesShouldFindWordWithSpace() {
        char[][] grid = 
            new char[][] {
                new char[] {'T', 'W', 'O', 'W', 'O', 'R', 'D', 'S'}
            };
        String expectedCoords = "0:0 0:7";
        assertEquals(expectedCoords, getWordCoordinates("TWO WORDS", grid));
    }
}
